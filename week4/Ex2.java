
public class Ex2 {
	public static void printMovies() {
		System.out.println("1. Fast & Furious 1");
		System.out.println("2. Fast & Furious 2");
		System.out.println("3. Fast & Furious 3");
		System.out.println("4. Fast & Furious 4");
		System.out.println("5. Fast & Furious 5");

	}

	public static void printMovies(int num, String name) {
		System.out.println(num + ". " + name);
	}

	public static void main(String[] args) {
		printMovies();
		System.out.println();
		printMovies(1, "Fast & Furious 1");
		printMovies(2, "Fast & Furious 2");
		printMovies(3, "Fast & Furious 3");
		printMovies(4, "Fast & Furious 4");
		printMovies(5, "Fast & Furious 5");
	}
}
