
public class Hw2 {
	public static void printMovies() {
		System.out.println("1. Fast & Furious 1");
		System.out.println("2. Fast & Furious 2");
		System.out.println("3. Fast & Furious 3");
		System.out.println("4. Fast & Furious 4");
		System.out.println("5. Fast & Furious 5");

	}

	public static void printMovies(int num, String name) {
		System.out.println(num + ". " + name);
	}

	public static void printMovies(String movies) {
		int position = movies.indexOf(',');
		int num = 1;
		while (position >= 0) {
			printMovies(num, movies.substring(0, position));
			movies = movies.substring(position + 2);
			position = movies.indexOf(',');
			num++;
		}
		printMovies(num, movies);
	}

	public static void main(String[] args) {
		printMovies("Fast & Furious 1, Fast & Furious 2, Fast & Furious 3, Fast & Furious 4, Fast & Furious 5");
	}
}
