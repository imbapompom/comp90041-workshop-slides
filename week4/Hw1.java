
public class Hw1 {
	private final static String movieList = "Fast & Furious 1 Fast & Furious 2 Fast & Furious 3 Fast & Furious 4 Fast & Furious 5";

	public static boolean isAFavourite(String movieName) {
		return movieList.contains(movieName);
	}

	public static void main(String[] args) {
		System.out.println(isAFavourite("Fast & Furious 1"));
		System.out.println(isAFavourite("Fast & Furious 6"));
	}
}
