/* *****************************************************************************
 *  Name:	Qilun Li
 *  Project:	COMP90041 Programming and Software Development
 *  			Workshop2 exercise 1
 *  Description:	Console input.
 **************************************************************************** */
import java.util.Scanner;
public class Ex1
{
	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		String str1 = keyboard.nextLine();
		System.out.println(str1.length());
		int position = str1.indexOf(" ");
		if (position >= 0)
			System.out.println(str1.substring(0,position));
		else
			System.out.println();
		System.out.println(str1.substring(position+1));		
	}
	
}