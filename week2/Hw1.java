/* *****************************************************************************
 *  Name:	Qilun Li
 *  Project:	COMP90041 Programming and Software Development
 *  			Workshop2 homework 1
 *  Description:	Command line input.
 **************************************************************************** */
public class Hw1 
{
	public static void main(String[] args)
	{
		System.out.println("\""+args[0]+"\"");
	}
}
