/* *****************************************************************************
 *  Name:	Qilun Li
 *  Project:	COMP90041 Programming and Software Development
 *  			Workshop5 exercise 1&2&3
 *  Description:	Immutable Movie class
 *  				Part of codes refer to Homework 1.
 **************************************************************************** */
public class Movie {
	private final String title;
	private final int rank;
	private final int runTime;
	private final Character mainCharacter; // for homework 1

	public Movie(String title, int rank, int runTime) {
		this.title = title;
		this.rank = rank;
		this.runTime = runTime;
		this.mainCharacter = null; // for homework 1
	}

	// for homework 1
	public Movie(String title, int rank, int runTime, Character mainCharactor) {
		this.title = title;
		this.rank = rank;
		this.runTime = runTime;
		this.mainCharacter = mainCharactor;
	}

	public Movie() {
		title = "";
		rank = 0;
		runTime = 0;
		mainCharacter = null; // for homework 1
	}

	public String toString() {
		if (mainCharacter != null)
			return rank + ". " + title + ". " + mainCharacter; // for homework 1
		else
			return rank + ". " + title;
	}

	public boolean equals(Movie otherMovie) {
		if (!this.title.equals(otherMovie.title)) {
			return false;
		}
		if (this.rank != otherMovie.rank) {
			return false;
		}
		if (this.runTime != otherMovie.runTime) {
			return false;
		}

		// for homework 1
		if (this.mainCharacter == null) {
			if (otherMovie.mainCharacter != null)
				return false;
		} else {
			if (!this.mainCharacter.equals(otherMovie.mainCharacter))
				return false;
		}

		return true;
	}

	public static void main(String[] args) {
		Movie movie1 = new Movie("Fast & Furious 1", 1, 90);
		Movie movie2 = new Movie("Fast & Furious 2", 2, 91);
		Movie movie3 = new Movie("Fast & Furious 3", 3, 92);
		Movie movie4 = new Movie("Fast & Furious 3", 3, 92);
		System.out.println(movie1);
		System.out.println(movie2);
		System.out.println(movie3);
		System.out.println(movie3.equals(movie1));
		System.out.println(movie3.equals(movie4));

		// for homework 1
		Character mainCharacter1 = new Character("Paul Walker", "Brian O'Conner", 10, "Fast & Furious 3");
		Movie movie5 = new Movie("Fast & Furious 3", 3, 92, mainCharacter1);
		Character mainCharacter2 = new Character("Paul Walker", "Brian O'Conner", 9, "Fast & Furious 3");
		Movie movie6 = new Movie("Fast & Furious 3", 3, 92, mainCharacter2);
		System.out.println(movie5);
		System.out.println(movie5.equals(movie6));
		System.out.println(movie5.equals(movie3));
	}
}
