/* *****************************************************************************
 *  Name:	Qilun Li
 *  Project:	COMP90041 Programming and Software Development
 *  			Workshop5 homework 1
 *  Description:	Immutable Character class
 **************************************************************************** */
public class Character {
	private final String characterName;
	private final String actorName;
	private final int rating;
	private final String movieName;

	public Character() {
		characterName = "";
		actorName = "";
		rating = 0;
		movieName = "";
	}

	public Character(String characterName, String actorName, int rating, String movieName) {
		this.characterName = characterName;
		this.actorName = actorName;
		this.rating = rating;
		this.movieName = movieName;
	}

	public String toString() {
		return "Charactor is " + characterName + ", actor is " + actorName + ", rating is " + rating + ", movie is "
				+ movieName + ".";
	}

	public boolean equals(Character otherCharacter) {
		if (otherCharacter == null)
			return false;
		if (!this.characterName.equals(otherCharacter.characterName)) {
			return false;
		}
		if (!this.actorName.equals(otherCharacter.actorName)) {
			return false;
		}
		if (this.rating != otherCharacter.rating) {
			return false;
		}
		if (!this.movieName.equals(otherCharacter.movieName)) {
			return false;
		}
		return true;
	}

}
