/* *****************************************************************************
 *  Name:	Qilun Li
 *  Project:	COMP90041 Programming and Software Development
 *  			Workshop3 exercise 1
 *  Description:	Switch statement
 **************************************************************************** */

public class Ex1 {
	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println("You must input one character at least.");
			System.exit(1);
		}
		switch (args[0])
		{
			case "N":
				System.out.println(0);
				break;
			case "E":
				System.out.println(90);
				break;
			case "S":
				System.out.println(180);
				break;
			case "W":
				System.out.println(270);
				break;
			default:
				System.out.println("Wrong command input!");
				System.exit(2);
		}
		
	}
}
