/* *****************************************************************************
 *  Name:	Qilun Li
 *  Project:	COMP90041 Programming and Software Development
 *  			Workshop2 homework 1
 *  Description:	Nested For loop.
 **************************************************************************** */

public class Hw1 
{
	public final static int MAX = 10;
	public static void main(String[] args) 
	{		
		System.out.print("  *  ");
		for (int j = 1; j <= MAX; j++) 
		{
			output(j);
		}
		for (int i = 1; i <= MAX; i++) 
		{
			outputline();
			System.out.printf("%4d ",i);
			for (int j = 1; j <= MAX; j++) 
			{
				output(i*j);
			}
		}
	}
	
	public static void output(int x)
	{
		System.out.printf("|%4d ",x);
	}
	
	public static void outputline()
	{
		System.out.println();
		for (int i = 1; i <= MAX; i++) 
		{
			System.out.print("-----+");
		}
		System.out.println("-----");
	}
}
