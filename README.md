This repository contains the workshop slides of COMP90041 Programming and software development offered at the University of Melbourne in Semester 2, 2019.

The workshop slides are based on the predecessor's slides.

The slide will be posted here at the end of each week. 

Please always refer to the official versions of the workshop solutions released on the LMS if you are looking for an 'authoritative' answer for any particular question

**If you want to get more workshop materials and reference answers, you can visit my colleague, as well as my friend, Chuang Wang's GitHub:**

https://github.com/chuangw46/COMP90041_Tutorial/blob/master/README.md